TARGET=thundergit

XPI_FILES=chrome.manifest install.rdf readme.txt \
	$(wildcard content/* locale/*/* skin/* defaults/*/*)

ID=$(shell sed -n -e 's/^.*<em:id>\(.*\)<\/em:id>.*/\1/p' -e '/em:name/q' \
	< install.rdf)

uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
ifneq (,$(findstring MINGW,$(uname_S)))
	APPDATA=$(USERPROFILE)/Application Data/Thunderbird/Profiles
	PROFILE_DIRECTORY=$(shell sh -c 'echo "$(APPDATA)"/*.default')
else
	ifeq (,$(shell ls -d "$(HOME)/.mozilla-thunderbird" 2>/dev/null))
		TBDIR=$(HOME)/.thunderbird
	else
		TBDIR=$(HOME)/.mozilla-thunderbird
	endif
	PROFILE_DIRECTORY=$(wildcard $(TBDIR)/*.default)
endif

$(TARGET).xpi: $(XPI_FILES)
	zip -9r $@ $^

symlink:
	ln -s "$(shell pwd)" "$(PROFILE_DIRECTORY)/extensions/$(ID)"

install: symlink

uninstall:
	rm -rf "$(PROFILE_DIRECTORY)/extensions/$(ID)"

reinstall: uninstall install
