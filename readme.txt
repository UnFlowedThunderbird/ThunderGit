This extension tries to make it easy to send inlined patches

In many projects (most notably, the Linux kernel project), it is considered
polite to send your patches inline so that you do not have to
switch/upgrade your mailer to respond to the (quoted) patch.

At the same time, the patch should be applicable, which means that it must
not be transformed, as is the case when using the flowed format (that some
consider to be a misspelt "flawed").

Default settings in Thunderbird make it hard to send patches that way.

This is where ThunderGit comes to the rescue.  Just select the
menu item "Send Patch(es)..." in the "File" menu, open your patch and
send it.  If the file contains mail headers, that is fine.
